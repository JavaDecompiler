package br.org.javadecompiler.decoder;

import java.io.ByteArrayInputStream;

public final class Utils {
	private Utils() {}

	public static String formatHex(int num, int size) {
		long val = ((long) num) & 0xFFFFFFFFL;
		return formatHex(val, size);
	}

	public static String formatHex(long num, int size) {
		String hex = Long.toHexString(num);
		if (size > hex.length()) {
			char[] chars = new char[size - hex.length()];
			for(int i = 0; i < chars.length; i++)
				chars[i] = '0';
			hex = new String(chars) + hex; 
		}
		return hex;
	}
	
	public static String printByteArray(byte[] bytes) {
		int available = 0;
		int offset = 0;
		
		ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
		StringBuffer result = new StringBuffer();

		
		bais.reset();
		available = bais.available();

		while(available > 0) {
			int count = 16;
			if (count > available) count = available;
			
			
			result.append(formatHex(offset, 8)).append(": ");
			for(int i = 0; i < count; i++) {
				result.append(formatHex(bais.read(), 2)).append(" ");
			}
			result.append("\n");
			offset += count;
			available -= count;
		}
		
		return result.toString();
	}
}