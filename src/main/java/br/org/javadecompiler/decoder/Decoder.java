package br.org.javadecompiler.decoder;

import br.org.javadecompiler.classfile.ConstantPool;
import br.org.javadecompiler.classfile.FieldOrMethod;
import br.org.javadecompiler.classfile.JavaClass;
import br.org.javadecompiler.classfile.Field;
import br.org.javadecompiler.classfile.Method;
import br.org.javadecompiler.classfile.attribute.Attribute;
import br.org.javadecompiler.classfile.attribute.AttributeOwner;
import br.org.javadecompiler.classfile.attribute.CodeAttribute;
import br.org.javadecompiler.classfile.attribute.ConstantValueAttribute;
import br.org.javadecompiler.classfile.attribute.DeprecatedAttribute;
import br.org.javadecompiler.classfile.attribute.ExceptionsAttribute;
import br.org.javadecompiler.classfile.attribute.InnerClassesAttribute;
import br.org.javadecompiler.classfile.attribute.LineNumberTableAttribute;
import br.org.javadecompiler.classfile.attribute.LocalVariableTableAttribute;
import br.org.javadecompiler.classfile.attribute.SignatureAttribute;
import br.org.javadecompiler.classfile.attribute.SourceFileAttribute;
import br.org.javadecompiler.classfile.attribute.SyntheticAttribute;
import br.org.javadecompiler.classfile.attribute.UnknownAttribute;
import br.org.javadecompiler.classfile.constant.ClassConstant;
import br.org.javadecompiler.classfile.constant.Constant;
import br.org.javadecompiler.classfile.constant.ConstantTag;
import br.org.javadecompiler.classfile.constant.DoubleConstant;
import br.org.javadecompiler.classfile.constant.FieldReferenceConstant;
import br.org.javadecompiler.classfile.constant.FloatConstant;
import br.org.javadecompiler.classfile.constant.IntegerConstant;
import br.org.javadecompiler.classfile.constant.InterfaceMethodReferenceConstant;
import br.org.javadecompiler.classfile.constant.LongConstant;
import br.org.javadecompiler.classfile.constant.MethodReferenceConstant;
import br.org.javadecompiler.classfile.constant.NameAndTypeConstant;
import br.org.javadecompiler.classfile.constant.PrimitiveTypeOrStringConstant;
import br.org.javadecompiler.classfile.constant.ReferenceConstant;
import br.org.javadecompiler.classfile.constant.StringConstant;
import br.org.javadecompiler.classfile.constant.UTF8Constant;
import br.org.javadecompiler.exception.ClassReaderException;
import br.org.javadecompiler.exception.DecoderException;
import br.org.javadecompiler.reader.ClassReader;

import org.apache.log4j.Logger;

public final class Decoder {
	private final Logger logger = Logger.getLogger(this.getClass());

	private final ClassReader reader;
	private final JavaClass ownerClass;
	
	private Decoder(JavaClass ownerClass, ClassReader reader) {
		this.ownerClass = ownerClass;
		this.reader = reader;
	}
	
	public static JavaClass decodeClass(ClassReader reader) throws DecoderException, ClassReaderException {
		Decoder decoder = new Decoder(new JavaClass(), reader);
		decoder.decodeClass();
		return decoder.ownerClass;
	}
	
	private void decodeClass() throws DecoderException, ClassReaderException {
		reader.reset();

		long magic = reader.readUnsignedInt();
		if (magic != 0xcafebabeL)
			throw new DecoderException("Invalid magic number: 0x" + Utils.formatHex(magic, 8));
		
		ownerClass.setMinorVersion(reader.readUnsignedShort());
		ownerClass.setMajorVersion(reader.readUnsignedShort());
		if (logger.isDebugEnabled())
			logger.debug("Class version: " + ownerClass.getMajorVersion() + "." + ownerClass.getMinorVersion());
		
		decodeConstantPool();
		
		ownerClass.setAccessFlags(reader.readUnsignedShort());
		
		int thisClassIndex = reader.readUnsignedShort();
		ownerClass.setThisClass((ClassConstant) ownerClass.getConstantPool().getConstant(thisClassIndex));
		int superClassIndex = reader.readUnsignedShort();
		if (superClassIndex != 0)
			ownerClass.setSuperClass((ClassConstant) ownerClass.getConstantPool().getConstant(superClassIndex));
		else {
			if (!"java/lang/Object".equals(ownerClass.getThisClass().getName().getValue()))
				throw new DecoderException("Class without a super class");
		}
		
		decodeInterfaces();
		
		ownerClass.setFields(decodeFields());
		ownerClass.setMethods(decodeMethods());
		ownerClass.setAttributes(decodeAttributes(ownerClass));
		
		if (reader.available() != 0)
			throw new DecoderException("End of stream expected");
		
	}
	
	private void decodeConstantPool() throws DecoderException, ClassReaderException {
		
		ConstantPool pool = new ConstantPool();
		Constant constants[] = new Constant[reader.readUnsignedShort()];
		pool.setConstants(constants);
		ownerClass.setConstantPool(pool);
		pool.setOwnerClass(ownerClass);
		if (logger.isDebugEnabled())
			logger.debug("Constant pool size: " + constants.length);
		
		reader.mark();
		if (logger.isDebugEnabled())
			logger.debug("Decoding constants: step 1");
		
		int index;
		for(index = 1; index < constants.length; index++) {
			Constant constant = decodeConstantFirstPass(index);
			constants[index] = constant;

			if ((constant instanceof LongConstant) || (constant instanceof DoubleConstant))
				index++;
		}
		if (index > constants.length)
			throw new DecoderException("Last constant cannot be of type Long or Double");


		reader.rollback();
		if (logger.isDebugEnabled()) {
			logger.debug("Decoding constants: step 2");
		}
		for(index = 1; index < constants.length; index++) {
			Constant constant = constants[index];
			decodeConstantSecondPass(constant, pool);

			if ((constant instanceof LongConstant) || (constant instanceof DoubleConstant))
				index++;
		}
	}
	
	private Constant decodeConstantFirstPass(int index) throws DecoderException, ClassReaderException {
		int tagValue = reader.readUnsignedByte();
		int size = 0;

		Constant constant = createNewConstantByTag(tagValue);
		
		constant.setOwnerClass(ownerClass);
		constant.setIndex(index);
		
		switch(tagValue) {
		case ConstantTag.STRING_TAG:
		case ConstantTag.CLASS_TAG:
			size = 2;
			break;
		case ConstantTag.NAMEANDTYPE_TAG:
		case ConstantTag.FIELDREF_TAG:
		case ConstantTag.INTERFACEMETHODREF_TAG:
		case ConstantTag.METHODREF_TAG:
		case ConstantTag.INTEGER_TAG:
		case ConstantTag.FLOAT_TAG:
			size = 4;
			break;
		case ConstantTag.DOUBLE_TAG:
		case ConstantTag.LONG_TAG:
			size = 8;
			break;
		case ConstantTag.UTF8_TAG:
			size = reader.readUnsignedShort();
			break;
		}
		reader.skip(size);
		return constant;
	}
	
	private void decodeConstantSecondPass(Constant constant, ConstantPool pool) throws DecoderException, ClassReaderException {
		reader.readUnsignedByte();
		if (constant instanceof ClassConstant) { 
			ClassConstant result = (ClassConstant) constant;
			int nameIndex = reader.readUnsignedShort();
			result.setName((UTF8Constant) pool.getConstant(nameIndex));
		} else if (constant instanceof ReferenceConstant) {
			ReferenceConstant result = (ReferenceConstant) constant;
			int classIndex = reader.readUnsignedShort();
			int nameAndTypeIndex = reader.readUnsignedShort();
			result.setReferenceClass((ClassConstant) pool.getConstant(classIndex));
			result.setNameAndTypeInfo((NameAndTypeConstant) pool.getConstant(nameAndTypeIndex));
		} else if (constant instanceof StringConstant) {
			StringConstant result = (StringConstant) constant;
			int stringIndex = reader.readUnsignedShort();
			result.setValue((UTF8Constant) pool.getConstant(stringIndex));
		} else if (constant instanceof IntegerConstant) {
			IntegerConstant result = (IntegerConstant) constant;
			result.setValue(reader.readSignedInt());
		} else if (constant instanceof FloatConstant) {
			FloatConstant result = (FloatConstant) constant;
			result.setValue(reader.readFloat());
		} else if (constant instanceof DoubleConstant) {
			DoubleConstant result = (DoubleConstant) constant;
			result.setValue(reader.readDouble());
		} else if (constant instanceof LongConstant) {
			LongConstant result = (LongConstant) constant;
			result.setValue(reader.readSignedLong());
		} else if (constant instanceof NameAndTypeConstant) {
			NameAndTypeConstant result = (NameAndTypeConstant) constant;
			int nameIndex = reader.readUnsignedShort();
			int descriptorIndex = reader.readUnsignedShort();
			result.setName((UTF8Constant) pool.getConstant(nameIndex));
			result.setDescriptor((UTF8Constant) pool.getConstant(descriptorIndex));
		} else if (constant instanceof UTF8Constant) {
			UTF8Constant result = (UTF8Constant) constant;
			result.setValue(reader.readUtf8());
		}
	}

	private Constant createNewConstantByTag(int tagValue) {
		switch(tagValue) {
		case ConstantTag.UTF8_TAG:
			return new UTF8Constant();
		case ConstantTag.INTEGER_TAG:
			return new IntegerConstant();
		case ConstantTag.FLOAT_TAG:
			return new FloatConstant();
		case ConstantTag.LONG_TAG:
			return new LongConstant();
		case ConstantTag.DOUBLE_TAG:
			return new DoubleConstant();
		case ConstantTag.CLASS_TAG:
			return new ClassConstant();
		case ConstantTag.STRING_TAG:
			return new StringConstant();
		case ConstantTag.FIELDREF_TAG:
			return new FieldReferenceConstant();
		case ConstantTag.METHODREF_TAG:
			return new MethodReferenceConstant();
		case ConstantTag.INTERFACEMETHODREF_TAG:
			return new InterfaceMethodReferenceConstant();
		case ConstantTag.NAMEANDTYPE_TAG:
			return new NameAndTypeConstant();
		}
		return null;
	}
	
	private void decodeInterfaces() throws DecoderException, ClassReaderException {
		ClassConstant []interfaces = new ClassConstant[reader.readUnsignedShort()];
		for(int i = 0; i < interfaces.length; i++) {
			int interfaceIndex = reader.readUnsignedShort(); 
			interfaces[i] = (ClassConstant) ownerClass.getConstantPool().getConstant(interfaceIndex);
		}
		ownerClass.setInterfaces(interfaces);
	}
	
	private Field[] decodeFields() throws DecoderException, ClassReaderException {
		Field[] result = new Field[reader.readUnsignedShort()];
		for(int i = 0; i < result.length; i++) {
			result[i] = new Field();
			decodeFieldOrMethod(result[i]);
		}
		return result;
	}
	
	private Method[] decodeMethods() throws DecoderException, ClassReaderException {
		Method[] result = new Method[reader.readUnsignedShort()];
		for(int i = 0; i < result.length; i++) {
			result[i] = new Method();
			decodeFieldOrMethod(result[i]);
		}
		return result;
	}
	
	private void decodeFieldOrMethod(FieldOrMethod fieldOrMethod) throws DecoderException, ClassReaderException {
		fieldOrMethod.setAccessFlags(reader.readUnsignedShort());
		int nameIndex = reader.readUnsignedShort();
		fieldOrMethod.setName((UTF8Constant) ownerClass.getConstantPool().getConstant(nameIndex));
		int descriptorIndex = reader.readUnsignedShort();
		fieldOrMethod.setDescriptor((UTF8Constant) ownerClass.getConstantPool().getConstant(descriptorIndex));
		fieldOrMethod.setAttributes(decodeAttributes(fieldOrMethod));
		fieldOrMethod.setOwnerClass(ownerClass);
	}
	
	private Attribute[] decodeAttributes(AttributeOwner owner) throws DecoderException, ClassReaderException {
		Attribute[] result = new Attribute[reader.readUnsignedShort()];
		for(int i = 0; i < result.length; i++) {
			result[i] = decodeAttribute(owner);
		}
			
		return result;
	}
	
	private Attribute decodeAttribute(AttributeOwner owner) throws DecoderException, ClassReaderException {
		int attributeNameIndex = reader.readUnsignedShort();
		UTF8Constant attributeName = (UTF8Constant) ownerClass.getConstantPool().getConstant(attributeNameIndex);
		String name = attributeName.getValue();
		Attribute result;
		if (Attribute.CONSTANT_VALUE_ATTRIBUTE.equals(name)) {
			result = decodeConstantValueAttribute();
		} else if (Attribute.CODE_ATTRIBUTE.equals(name)) {
			result = decodeCodeAttribute();
		} else if (Attribute.DEPRECATED_ATTRIBUTE.equals(name)) {
			result = decodeDeprecatedAttribute();
		} else if (Attribute.EXCEPTIONS_ATTRIBUTE.equals(name)) {
			result = decodeExceptionsAttribute();
		} else if (Attribute.INNER_CLASSES_ATTRIBUTE.equals(name)) {
			result = decodeInnerClassesAttribute();
		} else if (Attribute.LINE_NUMBER_TABLE_ATTRIBUTE.equals(name)) {
			result = decodeLineNumberTableAttribute();
		} else if (Attribute.LOCAL_VARIABLE_TABLE_ATTRIBUTE.equals(name)) {
			result = decodeLocalVariableTableAttribute();
		} else if (Attribute.SOURCE_FILE_ATTRIBUTE.equals(name)) {
			result = decodeSourceFileAttribute();
		} else if (Attribute.SYNTHETIC_ATTRIBUTE.equals(name)) {
			result = decodeSyntheticAttribute();
		} else if (Attribute.SIGNATURE_ATTRIBUTE.equals(name)) {
			result = decodeSignatureAttribute();
		} else {
			result = decodeUnknownAttribute();
			if (logger.isDebugEnabled()) {
				logger.debug("Unknown Attribute: " + attributeName.getValue());
			}
		}
		
		result.setAttributeName(attributeName);
		result.setOnwerClass(ownerClass);
		result.setOwner(owner);
		
		return result;
	}
	
	private ConstantValueAttribute decodeConstantValueAttribute() throws DecoderException, ClassReaderException {
		long attributeLength = reader.readUnsignedInt();
		if (attributeLength != 2)
			throw new DecoderException("Invalid ConstantValue attribute length");
		int constantValueIndex = reader.readUnsignedShort();
		ConstantValueAttribute result = new ConstantValueAttribute();
		result.setConstantValue((PrimitiveTypeOrStringConstant) ownerClass.getConstantPool().getConstant(constantValueIndex));
		return result;
	}
	
	private CodeAttribute decodeCodeAttribute() throws DecoderException, ClassReaderException {
		CodeAttribute result = new CodeAttribute();
		long attributeLength = reader.readUnsignedInt();
		int before = reader.position();
		result.setMaxStack(reader.readUnsignedShort());
		result.setMaxLocals(reader.readUnsignedShort());
		byte[] byteCode = new byte[(int) reader.readUnsignedInt()];
		reader.read(byteCode, 0, byteCode.length);
		result.setByteCode(byteCode);
		int numberOfExceptions = reader.readUnsignedShort();
		result.setExceptionTable(new CodeAttribute.CodeException[numberOfExceptions]);
		for(int i = 0; i < numberOfExceptions; i++) {
			CodeAttribute.CodeException codeException = result.new CodeException();
			codeException.setStartPc(reader.readUnsignedShort());
			codeException.setEndPc(reader.readUnsignedShort());
			codeException.setHandlerPc(reader.readUnsignedShort());
			int catchTypeIndex = reader.readUnsignedShort();
			if (catchTypeIndex != 0)
				codeException.setCatchType((ClassConstant) ownerClass.getConstantPool().getConstant(catchTypeIndex));
			result.getExceptionTable()[i] = codeException;
		}
		decodeAttributes(result);
		if (attributeLength != (reader.position() - before))
			throw new DecoderException("Invalid Code attribute length");
		return result;
	}
	
	private DeprecatedAttribute decodeDeprecatedAttribute() throws DecoderException, ClassReaderException {
		DeprecatedAttribute result = new DeprecatedAttribute();
		long attributeLength = reader.readUnsignedInt();
		if (attributeLength != 0)
			throw new DecoderException("Invalid Deprecated attribute length");
		return result;
	}
	
	private ExceptionsAttribute decodeExceptionsAttribute() throws DecoderException, ClassReaderException {
		ExceptionsAttribute result = new ExceptionsAttribute();
		long attributeLength = reader.readUnsignedInt();
		int numberOfExceptions = reader.readUnsignedShort();
		if (attributeLength != 2 * (numberOfExceptions + 1))
			throw new DecoderException("Invalid Exceptions attribute length");
		ClassConstant[] exceptions = new ClassConstant[numberOfExceptions];
		for(int i = 0; i < exceptions.length; i++) {
			int exceptionIndex = reader.readUnsignedShort();
			exceptions[i] = (ClassConstant) ownerClass.getConstantPool().getConstant(exceptionIndex);			
		}
		result.setExceptions(exceptions);
		return result;
	}
	
	private InnerClassesAttribute decodeInnerClassesAttribute() throws DecoderException, ClassReaderException {
		InnerClassesAttribute result = new InnerClassesAttribute();
		long attributeLength = reader.readUnsignedInt();
		int numberOfInnerClasses = reader.readUnsignedShort();
		if (attributeLength != 8 * numberOfInnerClasses + 2)
			throw new DecoderException("Invalid InnerClasses attribute length");
		result.setInnerClasses(new InnerClassesAttribute.InnerClass[numberOfInnerClasses]);
		for(int i = 0; i < numberOfInnerClasses; i++) {
			InnerClassesAttribute.InnerClass innerClass = result.new InnerClass();
			int innerClassInfoIndex = reader.readUnsignedShort();
			if (innerClassInfoIndex != 0)
				innerClass.setInnerClassInfo((ClassConstant) ownerClass.getConstantPool().getConstant(innerClassInfoIndex));
			int outerClassInfoIndex = reader.readUnsignedShort();
			if (outerClassInfoIndex != 0)
				innerClass.setOuterClassInfo((ClassConstant) ownerClass.getConstantPool().getConstant(outerClassInfoIndex));
			int nameIndex = reader.readUnsignedShort();
			if (nameIndex != 0)
				innerClass.setName((UTF8Constant) ownerClass.getConstantPool().getConstant(nameIndex));
			innerClass.setAccessFlags(reader.readUnsignedShort());
			result.getInnerClasses()[i] = innerClass;
		}
		return result;
	}
	
	private LineNumberTableAttribute decodeLineNumberTableAttribute() throws DecoderException, ClassReaderException {
		LineNumberTableAttribute result = new LineNumberTableAttribute();
		long attributeLength = reader.readUnsignedInt();
		int numberOfLines = reader.readUnsignedShort();
		if (attributeLength != 4 * numberOfLines + 2)
			throw new DecoderException("Invalid LineNumberTable attribute length");
		result.setLineNumberTable(new LineNumberTableAttribute.LineNumber[numberOfLines]);
		for(int i = 0; i < numberOfLines; i++) {
			LineNumberTableAttribute.LineNumber lineNumber = result.new LineNumber();
			lineNumber.setStartPc(reader.readUnsignedShort());
			lineNumber.setLineNumber(reader.readUnsignedShort());
			result.getLineNumberTable()[i] = lineNumber;
		}
		return result;
	}

	private LocalVariableTableAttribute decodeLocalVariableTableAttribute() throws DecoderException, ClassReaderException {
		LocalVariableTableAttribute result = new LocalVariableTableAttribute();
		long attributeLength = reader.readUnsignedInt();
		int numberOfLocalVariables = reader.readUnsignedShort();
		if (attributeLength != 10 * numberOfLocalVariables + 2)
			throw new DecoderException("Invalid LocalVariableTable attribute length");
		result.setLocalVariableTable(new LocalVariableTableAttribute.LocalVariable[numberOfLocalVariables]);
		for(int i = 0; i < numberOfLocalVariables; i++) {
			LocalVariableTableAttribute.LocalVariable localVariable = result.new LocalVariable();
			localVariable.setStartPc(reader.readUnsignedShort());
			localVariable.setLength(reader.readUnsignedShort());
			int nameIndex = reader.readUnsignedShort();
			localVariable.setName((UTF8Constant) ownerClass.getConstantPool().getConstant(nameIndex));
			int descriptorIndex = reader.readUnsignedShort();
			localVariable.setDescriptor((UTF8Constant) ownerClass.getConstantPool().getConstant(descriptorIndex));
			localVariable.setIndex(reader.readUnsignedShort());
			result.getLocalVariableTable()[i] = localVariable;
		}
		
		return result;
	}
	
	private SourceFileAttribute decodeSourceFileAttribute() throws DecoderException, ClassReaderException {
		SourceFileAttribute result = new SourceFileAttribute();
		long attributeLength = reader.readUnsignedInt();
		if (attributeLength != 2)
			throw new DecoderException("Invalid SourceFile attribute length");
		int sourceFileIndex = reader.readUnsignedShort();
		result.setSourceFile((UTF8Constant) ownerClass.getConstantPool().getConstant(sourceFileIndex));
		return result;
	}

	private SyntheticAttribute decodeSyntheticAttribute() throws DecoderException, ClassReaderException {
		SyntheticAttribute result = new SyntheticAttribute();
		long attributeLength = reader.readUnsignedInt();
		if (attributeLength != 0)
			throw new DecoderException("Invalid Synthetic attribute length");
		return result;
	}
	
	private SignatureAttribute decodeSignatureAttribute() throws DecoderException, ClassReaderException {
		SignatureAttribute result = new SignatureAttribute();
		long attributeLength = reader.readUnsignedInt();
		if (attributeLength != 2)
			throw new DecoderException("Invalid Signature attribute length");
		int signatureIndex = reader.readUnsignedShort();
		result.setSignature((UTF8Constant) ownerClass.getConstantPool().getConstant(signatureIndex));
		return result;
	}

	private UnknownAttribute decodeUnknownAttribute() throws DecoderException, ClassReaderException {
		UnknownAttribute result = new UnknownAttribute();
		long attributeLength = reader.readUnsignedInt();
		byte[] bytes = new byte[(int) attributeLength];
		reader.read(bytes, 0, bytes.length);
		result.setBytes(bytes);
		return result;
	}
}
