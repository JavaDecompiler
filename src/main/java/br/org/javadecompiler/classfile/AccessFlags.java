package br.org.javadecompiler.classfile;

public final class AccessFlags {
	public static final int
	  ACC_PUBLIC =       0x0001,
	  ACC_PRIVATE =      0x0002,
	  ACC_PROTECTED =    0x0004,
	  ACC_STATIC =       0x0008,
	  ACC_FINAL =        0x0010,
	  ACC_SUPER =        0x0020,
	  ACC_SYNCHRONIZED = 0x0020,
	  ACC_VOLATILE =     0x0040,
	  ACC_BRIDGE =       0x0040,
	  ACC_TRANSIENT =    0x0080,
	  ACC_VARARGS =      0x0080,
	  ACC_NATIVE =       0x0100,
	  ACC_INTERFACE =    0x0200,
	  ACC_ABSTRACT =     0x0400,
	  ACC_STRICT =       0x0800,
	  ACC_SYNTHETIC =    0x1000,
	  ACC_ANNOTATION =   0x2000,
	  ACC_ENUM =         0x4000;
	
	
	private AccessFlags() {}
	
	public static boolean isPublic(int flags) {
		return ((flags & ACC_PUBLIC) != 0);
	}
	
	public static boolean isPrivate(int flags) {
		return ((flags & ACC_PRIVATE) != 0);
	}
	
	public static boolean isProtected(int flags) {
		return ((flags & ACC_PROTECTED) != 0);
	}

	public static boolean isStatic(int flags) {
		return ((flags & ACC_STATIC) != 0);
	}
	
	public static boolean isFinal(int flags) {
		return ((flags & ACC_FINAL) != 0);
	}

	public static boolean isSuper(int flags) {
		return ((flags & ACC_SUPER) != 0);
	}
	
	public static boolean isSynchronized(int flags) {
		return ((flags & ACC_SYNCHRONIZED) != 0);
	}
	
	public static boolean isVolatile(int flags) {
		return ((flags & ACC_VOLATILE) != 0);
	}

	public static boolean isBridge(int flags) {
		return ((flags & ACC_BRIDGE) != 0);
	}

	public static boolean isTransient(int flags) {
		return ((flags & ACC_TRANSIENT) != 0);
	}

	public static boolean isVarargs(int flags) {
		return ((flags & ACC_VARARGS) != 0);
	}

	public static boolean isNative(int flags) {
		return ((flags & ACC_NATIVE) != 0);
	}
	
	public static boolean isInterface(int flags) {
		return ((flags & ACC_INTERFACE) != 0);
	}

	public static boolean isAbstract(int flags) {
		return ((flags & ACC_ABSTRACT) != 0);
	}

	public static boolean isStrict(int flags) {
		return ((flags & ACC_STRICT) != 0);
	}
	
	public static boolean isSynthetic(int flags) {
		return ((flags & ACC_SYNTHETIC) != 0);
	}
	
	public static boolean isAnnotation(int flags) {
		return ((flags & ACC_ANNOTATION) != 0);
	}

	public static boolean isEnum(int flags) {
		return ((flags & ACC_ENUM) != 0);
	}
}
