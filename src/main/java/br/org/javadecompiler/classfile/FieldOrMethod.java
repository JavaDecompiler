package br.org.javadecompiler.classfile;

import java.io.Serializable;

import br.org.javadecompiler.classfile.attribute.Attribute;
import br.org.javadecompiler.classfile.attribute.AttributeOwner;
import br.org.javadecompiler.classfile.constant.UTF8Constant;

public abstract class FieldOrMethod implements Serializable, AttributeOwner {
	private JavaClass ownerClass;
	
	private int accessFlags;
	private UTF8Constant name;
	private UTF8Constant descriptor;
	private Attribute[] attributes;
	
	public void setOwnerClass(JavaClass ownerClass) {
		this.ownerClass = ownerClass;
	}
	public JavaClass getOwnerClass() {
		return ownerClass;
	}

	public void setAccessFlags(int accessFlags) {
		this.accessFlags = accessFlags;
	}
	public int getAccessFlags() {
		return accessFlags;
	}
	public void setName(UTF8Constant name) {
		this.name = name;
	}
	public UTF8Constant getName() {
		return name;
	}
	public void setDescriptor(UTF8Constant descriptor) {
		this.descriptor = descriptor;
	}
	public UTF8Constant getDescriptor() {
		return descriptor;
	}
	public void setAttributes(Attribute[] attributes) {
		this.attributes = attributes;
	}
	public Attribute[] getAttributes() {
		return attributes;
	}
	
}
