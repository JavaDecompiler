package br.org.javadecompiler.classfile;

import java.io.Serializable;
import java.util.Enumeration;

import br.org.javadecompiler.classfile.constant.Constant;
import br.org.javadecompiler.classfile.constant.DoubleConstant;
import br.org.javadecompiler.classfile.constant.LongConstant;

public class ConstantPool implements Serializable {
	private static final long serialVersionUID = -1662344335531128139L;

	private JavaClass ownerClass;
	private Constant[] constants;

	public void setOwnerClass(JavaClass ownerClass) {
		this.ownerClass = ownerClass;
	}

	public JavaClass getOwnerClass() {
		return ownerClass;
	}

	public void setConstants(Constant[] constants) {
		this.constants = constants;
	}

	public Constant[] getConstants() {
		return constants;
	}
	
	public Constant getConstant(int index) {
		Constant result = constants[index];
		if (result == null) throw new NullPointerException("Invalid constant pool index");
		return result;
	}
	
	public Enumeration enumerate() {
		return new ConstantPoolEnumeration(getConstants());
	}

	private static class ConstantPoolEnumeration implements Enumeration {
		private final Constant[] constants;
		private int index;
		
		public ConstantPoolEnumeration(Constant[] constants) {
			if (constants == null || constants.length <= 1)
				throw new IllegalArgumentException("Invalid constants");
			this.constants = constants;
			index = 1;
		}

		public boolean hasMoreElements() {
			return (index != constants.length);
		}

		public Object nextElement() {
			Constant result = constants[index++];
			if (result instanceof LongConstant || result instanceof DoubleConstant)
				index++;
			return result;
		}		
	}
	
}
