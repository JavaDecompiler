package br.org.javadecompiler.classfile.attribute;

import java.io.Serializable;

import br.org.javadecompiler.classfile.constant.ClassConstant;

public class CodeAttribute extends Attribute implements AttributeOwner {

	private static final long serialVersionUID = 1354706417062530459L;
	private int maxStack;
	private int maxLocals;
	private byte[] byteCode;
	private CodeException[] exceptionTable;
	private Attribute[] attributes;
	
	public void setMaxStack(int maxStack) {
		this.maxStack = maxStack;
	}
	public int getMaxStack() {
		return maxStack;
	}


	public void setMaxLocals(int maxLocals) {
		this.maxLocals = maxLocals;
	}
	public int getMaxLocals() {
		return maxLocals;
	}


	public void setByteCode(byte[] byteCode) {
		this.byteCode = byteCode;
	}
	public byte[] getByteCode() {
		return byteCode;
	}
	
	public short getByteCode(int pos) {
		byte bc = getByteCode()[pos];
		if (bc < 0)
			return (short) (256 + bc) ;
		return bc;
	}


	public void setExceptionTable(CodeException[] exceptionTable) {
		this.exceptionTable = exceptionTable;
	}
	public CodeException[] getExceptionTable() {
		return exceptionTable;
	}


	public void setAttributes(Attribute[] attributes) {
		this.attributes = attributes;
	}
	public Attribute[] getAttributes() {
		return attributes;
	}
	
	public class CodeException implements Serializable {

		private static final long serialVersionUID = -1056082784248977526L;
		private int startPc;
		private int endPc;
		private int handlerPc;
		private ClassConstant catchType;
		
		public void setStartPc(int startPc) {
			this.startPc = startPc;
		}
		public int getStartPc() {
			return startPc;
		}
		public void setEndPc(int endPc) {
			this.endPc = endPc;
		}
		public int getEndPc() {
			return endPc;
		}
		public void setHandlerPc(int handlerPc) {
			this.handlerPc = handlerPc;
		}
		public int getHandlerPc() {
			return handlerPc;
		}
		public void setCatchType(ClassConstant catchType) {
			this.catchType = catchType;
		}
		public ClassConstant getCatchType() {
			return catchType;
		}
		
	}


}
