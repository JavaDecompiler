package br.org.javadecompiler.classfile.attribute;

import java.io.Serializable;

import br.org.javadecompiler.classfile.constant.UTF8Constant;

public class LocalVariableTableAttribute extends Attribute {

	private static final long serialVersionUID = 7505170653008105411L;
	private LocalVariable[] localVariableTable;
	
	public void setLocalVariableTable(LocalVariable[] localVariableTable) {
		this.localVariableTable = localVariableTable;
	}

	public LocalVariable[] getLocalVariableTable() {
		return localVariableTable;
	}

	public class LocalVariable implements Serializable {
		private static final long serialVersionUID = -2999775504638915189L;
		private int startPc;
		private int length;
		private UTF8Constant name;
		private UTF8Constant descriptor;
		private int index;
		
		public void setStartPc(int startPc) {
			this.startPc = startPc;
		}
		public int getStartPc() {
			return startPc;
		}
		public void setLength(int length) {
			this.length = length;
		}
		public int getLength() {
			return length;
		}
		public void setName(UTF8Constant name) {
			this.name = name;
		}
		public UTF8Constant getName() {
			return name;
		}
		public void setDescriptor(UTF8Constant descriptor) {
			this.descriptor = descriptor;
		}
		public UTF8Constant getDescriptor() {
			return descriptor;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		public int getIndex() {
			return index;
		}
		
	}


}
