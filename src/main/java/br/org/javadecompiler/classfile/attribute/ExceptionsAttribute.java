package br.org.javadecompiler.classfile.attribute;

import br.org.javadecompiler.classfile.constant.ClassConstant;

public class ExceptionsAttribute extends Attribute {

	private static final long serialVersionUID = -6509815172357331900L;
	private ClassConstant[] exceptions;
	
	public void setExceptions(ClassConstant[] exceptions) {
		this.exceptions = exceptions;
	}
	public ClassConstant[] getExceptions() {
		return exceptions;
	}

}
