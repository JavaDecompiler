package br.org.javadecompiler.classfile.attribute;

import java.io.Serializable;

public class LineNumberTableAttribute extends Attribute {

	private static final long serialVersionUID = 8958463818511281120L;
	private LineNumber[] lineNumberTable;
	
	public void setLineNumberTable(LineNumber[] lineNumberTable) {
		this.lineNumberTable = lineNumberTable;
	}
	public LineNumber[] getLineNumberTable() {
		return lineNumberTable;
	}

	public class LineNumber implements Serializable {
		private static final long serialVersionUID = 7267458737051584831L;
		private int startPc;
		private int lineNumber;

		public void setStartPc(int startPc) {
			this.startPc = startPc;
		}
		public int getStartPc() {
			return startPc;
		}
		public void setLineNumber(int lineNumber) {
			this.lineNumber = lineNumber;
		}
		public int getLineNumber() {
			return lineNumber;
		}
		
		
	}
}
