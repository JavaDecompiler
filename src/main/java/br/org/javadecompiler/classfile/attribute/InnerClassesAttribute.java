package br.org.javadecompiler.classfile.attribute;

import java.io.Serializable;

import br.org.javadecompiler.classfile.constant.ClassConstant;
import br.org.javadecompiler.classfile.constant.UTF8Constant;

public class InnerClassesAttribute extends Attribute {

	private static final long serialVersionUID = 4717761538102075931L;
	private InnerClass[] innerClasses;

	public void setInnerClasses(InnerClass[] innerClasses) {
		this.innerClasses = innerClasses;
	}

	public InnerClass[] getInnerClasses() {
		return innerClasses;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Inner classes: ");
		for(int i = 0; i < getInnerClasses().length; i++) {
			if (i != 0) sb.append(", ");
			if (getInnerClasses()[i].getName() != null)
				sb.append(getInnerClasses()[i].getName().getValue());
			else
				sb.append("Anonymous??");
		}
		return sb.toString();
	}

	public class InnerClass implements Serializable {
		private static final long serialVersionUID = -1796892508153314952L;
		private ClassConstant innerClassInfo;
		private ClassConstant outerClassInfo;
		private UTF8Constant name;
		private int accessFlags;
		
		public void setInnerClassInfo(ClassConstant innerClassInfo) {
			this.innerClassInfo = innerClassInfo;
		}
		public ClassConstant getInnerClassInfo() {
			return innerClassInfo;
		}
		public void setOuterClassInfo(ClassConstant outerClassInfo) {
			this.outerClassInfo = outerClassInfo;
		}
		public ClassConstant getOuterClassInfo() {
			return outerClassInfo;
		}
		public void setName(UTF8Constant name) {
			this.name = name;
		}
		public UTF8Constant getName() {
			return name;
		}
		public void setAccessFlags(int accessFlags) {
			this.accessFlags = accessFlags;
		}
		public int getAccessFlags() {
			return accessFlags;
		}
		
	}
}
