package br.org.javadecompiler.classfile.attribute;

public class UnknownAttribute extends Attribute {

	private static final long serialVersionUID = 6206773123688632961L;
	
	private byte[] bytes;

	public void setBytes(byte[] bytes) {
		this.bytes = bytes;
	}

	public byte[] getBytes() {
		return bytes;
	}
	

}
