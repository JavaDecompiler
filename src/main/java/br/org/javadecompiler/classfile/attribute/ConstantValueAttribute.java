package br.org.javadecompiler.classfile.attribute;

import br.org.javadecompiler.classfile.constant.PrimitiveTypeOrStringConstant;

public class ConstantValueAttribute extends Attribute {

	private static final long serialVersionUID = 1449314375437438583L;
	private PrimitiveTypeOrStringConstant constantValue;
	
	public void setConstantValue(PrimitiveTypeOrStringConstant constantValue) {
		this.constantValue = constantValue;
	}
	public PrimitiveTypeOrStringConstant getConstantValue() {
		return constantValue;
	}
	
	
}
