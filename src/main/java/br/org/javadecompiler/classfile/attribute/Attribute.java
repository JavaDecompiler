package br.org.javadecompiler.classfile.attribute;

import java.io.Serializable;

import br.org.javadecompiler.classfile.JavaClass;
import br.org.javadecompiler.classfile.constant.UTF8Constant;

public abstract class Attribute implements Serializable {
	public static String ANNOTATION_DEFAULT_ATTRIBUTE = "AnnotationDefault";
	public static String CODE_ATTRIBUTE = "Code";
	public static String CONSTANT_VALUE_ATTRIBUTE = "ConstantValue";
	public static String DEPRECATED_ATTRIBUTE = "Deprecated";
	public static String ENCLOSING_METHOD_ATTRIBUTE = "EnclosingMethod";
	public static String EXCEPTIONS_ATTRIBUTE = "Exceptions";
	public static String INNER_CLASSES_ATTRIBUTE = "InnerClasses";
	public static String LINE_NUMBER_TABLE_ATTRIBUTE = "LineNumberTable";
	public static String LOCAL_VARIABLE_TABLE_ATTRIBUTE = "LocalVariableTable";
	public static String LOCAL_VARIABLE_TYPE_TABLE_ATTRIBUTE = "LocalVariableTypeTable";
	public static String RUNTIME_INVISIBLE_ANNOTATIONS_ATTRIBUTE = "RuntimeInvisibleAnnotationsAttribute";
	public static String RUNTIME_INVISIBLE_PARAMETER_ANNOTATIONS_ATTRIBUTE = "RuntimeInvisibleParameterAnnotationsAttribute";
	public static String RUNTIME_VISIBLE_ANNOTATIONS_ATTRIBUTE = "RuntimeVisibleAnnotationsAttribute";
	public static String RUNTIME_VISIBLE_PARAMETER_ANNOTATIONS_ATTRIBUTE = "RuntimeVisibleParameterAnnotationsAttribute";
	public static String SIGNATURE_ATTRIBUTE = "Signature";
	public static String SOURCE_DEBUG_EXTENSION_ATTRIBUTE = "SourceDebugExtension";
	public static String SOURCE_FILE_ATTRIBUTE = "SourceFile";
	public static String STACK_MAP_TABLE_ATTRIBUTE = "StackMapTable";
	public static String SYNTHETIC_ATTRIBUTE = "Synthetic";
		
	
	private JavaClass ownerClass;
	private UTF8Constant attributeName;
	private AttributeOwner owner;

	public void setOnwerClass(JavaClass ownerClass) {
		this.ownerClass = ownerClass;
	}
	
	public JavaClass getOnwerClass() {
		return ownerClass;
	}
	
	public void setAttributeName(UTF8Constant attributeName) {
		this.attributeName = attributeName;
	}

	public UTF8Constant getAttributeName() {
		return attributeName;
	}
	
	public AttributeOwner getOwner() {
		return owner;
	}
	
	public void setOwner(AttributeOwner owner) {
		this.owner = owner;
	}
}
