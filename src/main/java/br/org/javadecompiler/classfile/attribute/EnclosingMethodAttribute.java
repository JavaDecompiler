package br.org.javadecompiler.classfile.attribute;

import br.org.javadecompiler.classfile.constant.ClassConstant;
import br.org.javadecompiler.classfile.constant.NameAndTypeConstant;

public class EnclosingMethodAttribute extends Attribute {

	private static final long serialVersionUID = 4451871846804871923L;

	private ClassConstant enclosingClass;
	private NameAndTypeConstant enclosingMethod;
	
	public void setEnclosingClass(ClassConstant enclosingClass) {
		this.enclosingClass = enclosingClass;
	}
	public ClassConstant getEnclosingClass() {
		return enclosingClass;
	}
	public void setEnclosingMethod(NameAndTypeConstant enclosingMethod) {
		this.enclosingMethod = enclosingMethod;
	}
	public NameAndTypeConstant getEnclosingMethod() {
		return enclosingMethod;
	}
	
}
