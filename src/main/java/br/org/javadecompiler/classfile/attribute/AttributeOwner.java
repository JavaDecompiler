package br.org.javadecompiler.classfile.attribute;

public interface AttributeOwner {
	public Attribute[] getAttributes();
}
