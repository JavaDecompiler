package br.org.javadecompiler.classfile.attribute;

import br.org.javadecompiler.classfile.constant.UTF8Constant;

public class SignatureAttribute extends Attribute {

	private static final long serialVersionUID = -470127468914314422L;
	
	private UTF8Constant signature;

	public void setSignature(UTF8Constant signature) {
		this.signature = signature;
	}

	public UTF8Constant getSignature() {
		return signature;
	}

}
