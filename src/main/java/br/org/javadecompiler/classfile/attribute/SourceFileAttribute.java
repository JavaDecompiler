package br.org.javadecompiler.classfile.attribute;

import br.org.javadecompiler.classfile.constant.UTF8Constant;

public class SourceFileAttribute extends Attribute {

	private static final long serialVersionUID = 553441759718956796L;
	private UTF8Constant sourceFile;

	public void setSourceFile(UTF8Constant sourceFile) {
		this.sourceFile = sourceFile;
	}
	public UTF8Constant getSourceFile() {
		return sourceFile;
	}
}
