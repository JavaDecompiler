package br.org.javadecompiler.classfile;

import java.io.Serializable;

import br.org.javadecompiler.classfile.attribute.Attribute;
import br.org.javadecompiler.classfile.attribute.AttributeOwner;
import br.org.javadecompiler.classfile.constant.ClassConstant;

public class JavaClass implements Serializable, AttributeOwner {
	private static final long serialVersionUID = -1157351891998240629L;

	private int minorVersion;
	private int majorVersion;
	private ConstantPool constantPool;
	
	private int accessFlags;
	private ClassConstant thisClass;
	private ClassConstant superClass;
	private ClassConstant[] interfaces;
	private Field[] fields;
	private Method[] methods;
	private Attribute[] attributes;
	
	public void setMinorVersion(int minorVersion) {
		this.minorVersion = minorVersion;
	}
	public int getMinorVersion() {
		return minorVersion;
	}
	public void setMajorVersion(int majorVersion) {
		this.majorVersion = majorVersion;
	}
	public int getMajorVersion() {
		return majorVersion;
	}
	public void setConstantPool(ConstantPool constantPool) {
		this.constantPool = constantPool;
	}
	public ConstantPool getConstantPool() {
		return constantPool;
	}
	public void setAccessFlags(int accessFlags) {
		this.accessFlags = accessFlags;
	}
	public int getAccessFlags() {
		return accessFlags;
	}
	public void setThisClass(ClassConstant thisClass) {
		this.thisClass = thisClass;
	}
	public ClassConstant getThisClass() {
		return thisClass;
	}
	public void setSuperClass(ClassConstant superClass) {
		this.superClass = superClass;
	}
	public ClassConstant getSuperClass() {
		return superClass;
	}
	public void setInterfaces(ClassConstant[] interfaces) {
		this.interfaces = interfaces;
	}
	public ClassConstant[] getInterfaces() {
		return interfaces;
	}
	public void setFields(Field[] fields) {
		this.fields = fields;
	}
	public Field[] getFields() {
		return fields;
	}
	public void setMethods(Method[] methods) {
		this.methods = methods;
	}
	public Method[] getMethods() {
		return methods;
	}
	public void setAttributes(Attribute[] attributes) {
		this.attributes = attributes;
	}
	public Attribute[] getAttributes() {
		return attributes;
	}
	
}
