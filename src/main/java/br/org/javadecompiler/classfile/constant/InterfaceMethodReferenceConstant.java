package br.org.javadecompiler.classfile.constant;

public class InterfaceMethodReferenceConstant extends ReferenceConstant {

	private static final long serialVersionUID = -475809502119021848L;

	public String toString() {
		StringBuffer sb = new StringBuffer("IMethod     ");
		super.toString(sb);
		return sb.toString();
	}

}
