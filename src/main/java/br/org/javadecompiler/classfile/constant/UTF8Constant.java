package br.org.javadecompiler.classfile.constant;

public class UTF8Constant extends Constant {

	private static final long serialVersionUID = 8776583682785972948L;
	private String value;

	public void setValue(String value) {
		this.value = value;
	}
	public String getValue() {
		return value;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Asciz       ").append(getValue()).append(";");
		return sb.toString();
	}
}
