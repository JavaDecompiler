package br.org.javadecompiler.classfile.constant;

public class StringConstant extends PrimitiveTypeOrStringConstant {

	private static final long serialVersionUID = -8444487161992340871L;
	private UTF8Constant value;
	public void setValue(UTF8Constant value) {
		this.value = value;
	}
	public UTF8Constant getValue() {
		return value;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("String      ");
		if (getValue() != null) {
			sb.append("#").append(getValue().getIndex());
			sb.append(";");
			if (getValue().getValue() != null) {
				sb.append("\t// ").append(getValue().getValue());
			}
		}
		return sb.toString();
	}
	
}
