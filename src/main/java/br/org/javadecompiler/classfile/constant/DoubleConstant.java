package br.org.javadecompiler.classfile.constant;

public class DoubleConstant extends PrimitiveTypeOrStringConstant {

	private static final long serialVersionUID = -3106613805226612545L;
	private double value;

	public void setValue(double value) {
		this.value = value;
	}
	public double getValue() {
		return value;
	}
	
	public String toString() {
		return "double " + Double.toString(getValue());
	}

}
