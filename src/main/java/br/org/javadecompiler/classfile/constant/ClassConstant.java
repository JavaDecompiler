package br.org.javadecompiler.classfile.constant;

public class ClassConstant extends Constant {

	private static final long serialVersionUID = -3408523207479412863L;
	private UTF8Constant name;

	public void setName(UTF8Constant name) {
		this.name = name;
	}
	public UTF8Constant getName() {
		return name;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("class       ");
		if (getName() != null) {
			sb.append("#").append(getName().getIndex());
			sb.append(";");
			if (getName().getValue() != null)
				sb.append("\t// ").append(getName().getValue());
		}
		return sb.toString();
	}
	
}
