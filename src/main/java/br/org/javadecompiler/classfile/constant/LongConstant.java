package br.org.javadecompiler.classfile.constant;

public class LongConstant extends PrimitiveTypeOrStringConstant {

	private static final long serialVersionUID = 5822305950396963984L;
	private long value;

	public void setValue(long value) {
		this.value = value;
	}
	public long getValue() {
		return value;
	}
	
	public String toString() {
		return "long        " + getValue();
	}

}
