package br.org.javadecompiler.classfile.constant;

public abstract class ReferenceConstant extends Constant {
	private ClassConstant referenceClass;
	private NameAndTypeConstant nameAndTypeInfo;
	
	public void setReferenceClass(ClassConstant referenceClass) {
		this.referenceClass = referenceClass;
	}
	public ClassConstant getReferenceClass() {
		return referenceClass;
	}
	public void setNameAndTypeInfo(NameAndTypeConstant nameAndTypeInfo) {
		this.nameAndTypeInfo = nameAndTypeInfo;
	}
	public NameAndTypeConstant getNameAndTypeInfo() {
		return nameAndTypeInfo;
	}
	
	protected void toString(StringBuffer sb) {
		if (getReferenceClass() != null && getNameAndTypeInfo() != null) {
			sb.append("#").append(getReferenceClass().getIndex()).append(".#").append(getNameAndTypeInfo().getIndex());
			sb.append(";");
			if (getReferenceClass().getName() != null && getReferenceClass().getName().getValue() != null &&
				getNameAndTypeInfo().getName() != null && getNameAndTypeInfo().getName().getValue() != null &&
				getNameAndTypeInfo().getDescriptor() != null && getNameAndTypeInfo().getDescriptor().getValue() != null) {
				
				sb.append("\t// ").append(getReferenceClass().getName().getValue()).append(".");
				sb.append(getNameAndTypeInfo().getName().getValue()).append(":").append(getNameAndTypeInfo().getDescriptor().getValue());
			}
		}
	}

}
