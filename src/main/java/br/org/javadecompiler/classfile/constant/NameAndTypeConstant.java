package br.org.javadecompiler.classfile.constant;

public class NameAndTypeConstant extends Constant {

	private static final long serialVersionUID = -4677560329948926436L;
	private UTF8Constant name;
	private UTF8Constant descriptor;
	
	public void setName(UTF8Constant name) {
		this.name = name;
	}
	public UTF8Constant getName() {
		return name;
	}
	public void setDescriptor(UTF8Constant descriptor) {
		this.descriptor = descriptor;
	}
	public UTF8Constant getDescriptor() {
		return descriptor;
	}

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("NameAndType ");
		if (getName() != null && getDescriptor() != null) {
			sb.append("#").append(getName().getIndex()).append(":").append(getDescriptor().getIndex());
			sb.append(";");
			if (getName().getValue() != null && getDescriptor().getValue() != null) {
				sb.append("\t// ");
				sb.append(getName().getValue()).append(":").append(getDescriptor().getValue());
			}
		}
		return sb.toString();
	}
	
	
}
