package br.org.javadecompiler.classfile.constant;

import java.io.Serializable;

import br.org.javadecompiler.classfile.JavaClass;

public abstract class Constant implements Serializable {
	private JavaClass ownerClass;
	private int index;

	public void setOwnerClass(JavaClass ownerClass) {
		this.ownerClass = ownerClass;
	}

	public JavaClass getOwnerClass() {
		return ownerClass;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getIndex() {
		return index;
	}
}
