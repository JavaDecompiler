package br.org.javadecompiler.classfile.constant;

public class FloatConstant extends PrimitiveTypeOrStringConstant {

	private static final long serialVersionUID = 1088671914329723191L;
	private float value;
	
	public void setValue(float value) {
		this.value = value;
	}
	public float getValue() {
		return value;
	}
	
	public String toString() {
		return "float " + Float.toString(getValue());
	}

}
