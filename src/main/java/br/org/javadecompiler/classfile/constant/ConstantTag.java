package br.org.javadecompiler.classfile.constant;

public final class ConstantTag {
	public static final int
	  UTF8_TAG =                1,
	  INTEGER_TAG =             3,
	  FLOAT_TAG =               4,
	  LONG_TAG =                5,
	  DOUBLE_TAG =              6,
	  CLASS_TAG =               7,
	  STRING_TAG =              8,
	  FIELDREF_TAG =            9,
	  METHODREF_TAG =          10,
	  INTERFACEMETHODREF_TAG = 11,
	  NAMEANDTYPE_TAG =        12;
}
