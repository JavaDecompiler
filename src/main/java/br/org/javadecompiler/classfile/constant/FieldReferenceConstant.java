package br.org.javadecompiler.classfile.constant;

public class FieldReferenceConstant extends ReferenceConstant {

	private static final long serialVersionUID = 4083714819076552775L;
	
	public String toString() {
		StringBuffer sb = new StringBuffer("Field       ");
		super.toString(sb);
		return sb.toString();
	}

}
