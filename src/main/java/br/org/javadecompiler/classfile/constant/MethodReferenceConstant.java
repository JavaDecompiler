package br.org.javadecompiler.classfile.constant;

public class MethodReferenceConstant extends ReferenceConstant {

	private static final long serialVersionUID = 8929564164929661966L;

	public String toString() {
		StringBuffer sb = new StringBuffer("Method      ");
		super.toString(sb);
		return sb.toString();
	}
	
}
