package br.org.javadecompiler.classfile.constant;

public class IntegerConstant extends PrimitiveTypeOrStringConstant {

	private static final long serialVersionUID = 5027935362316032891L;
	
	private int value;

	public void setValue(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	public String toString() {
		return "int         " + getValue();
	}

}
