package br.org.javadecompiler.disassembler;

import java.util.Vector;

import br.org.javadecompiler.decoder.Utils;
import br.org.javadecompiler.classfile.AccessFlags;
import br.org.javadecompiler.classfile.Field;
import br.org.javadecompiler.classfile.JavaClass;
import br.org.javadecompiler.classfile.Method;
import br.org.javadecompiler.classfile.attribute.Attribute;
import br.org.javadecompiler.classfile.attribute.CodeAttribute;
import br.org.javadecompiler.classfile.attribute.EnclosingMethodAttribute;
import br.org.javadecompiler.classfile.attribute.InnerClassesAttribute;
import br.org.javadecompiler.classfile.attribute.SignatureAttribute;
import br.org.javadecompiler.classfile.attribute.SourceDebugExtensionAttribute;
import br.org.javadecompiler.classfile.attribute.SourceFileAttribute;
import br.org.javadecompiler.classfile.constant.ClassConstant;
import br.org.javadecompiler.classfile.constant.UTF8Constant;
import br.org.javadecompiler.insn.BytecodeInstructions;

public class JasminDisassembler {

	public String disassembleClass(JavaClass clazz) {
		StringBuffer sb = new StringBuffer();
		disassembleClass(clazz, sb);
		return sb.toString();
	}
		
	private void disassembleClass(JavaClass clazz, StringBuffer sb) {
		sb.append(".bytecode ").append(clazz.getMajorVersion())
		  .append(".").append(clazz.getMinorVersion()).append("\n");
		
		SourceFileAttribute sourceFile = null;
		SignatureAttribute signature = null;
		EnclosingMethodAttribute enclosingMethod = null;
		Vector debugExtensions = new Vector();
		InnerClassesAttribute innerClasses = null;
		for(int i = 0; i < clazz.getAttributes().length; i++) {
			Attribute attribute = clazz.getAttributes()[i];
			if (attribute instanceof SourceFileAttribute)
				sourceFile = (SourceFileAttribute) attribute;
			else if (attribute instanceof SignatureAttribute)
				signature = (SignatureAttribute) attribute;
			else if (attribute instanceof SourceDebugExtensionAttribute)
				debugExtensions.addElement(attribute);
			else if (attribute instanceof InnerClassesAttribute)
				innerClasses = (InnerClassesAttribute) attribute;
			else if (attribute instanceof EnclosingMethodAttribute)
				enclosingMethod = (EnclosingMethodAttribute) attribute;
		}
		
		if (sourceFile != null) disassembleSourceFileAttribute(sourceFile, sb);

		sb.append(".class ");
		disassembleClassAccessFlags(clazz.getAccessFlags(), sb);
		disassembleClassConstant(clazz.getThisClass(), sb);
		sb.append("\n");
		
		if (clazz.getSuperClass() != null) {
			sb.append(".super ");
			disassembleClassConstant(clazz.getSuperClass(), sb);
			sb.append("\n");
		}
		
		disassembleInterfaces(clazz.getInterfaces(), sb);
		
		if (signature != null)
			disassembleSignatureAttribute(signature, sb);
		
		if (enclosingMethod != null)
			disassembleEnclosingMethodAttribute(enclosingMethod, sb);
		
		
		
		if (clazz.getFields().length > 0) {
			sb.append("\n\n");
			for (int i = 0; i < clazz.getFields().length; i++) {
				disassembleField(clazz.getFields()[i], sb);
				sb.append("\n");
			}
		}
		
		
		
		if (clazz.getMethods().length > 0) {
			sb.append("\n\n");
			for (int i = 0; i < clazz.getMethods().length; i++) {
				disassembleMethod(clazz.getMethods()[i], sb);
				sb.append("\n");
			}
		}
		
	}
	
	private void disassembleClassAccessFlags(int flags, StringBuffer sb) {
		if (AccessFlags.isPublic(flags))
			sb.append("public ");
		if (AccessFlags.isFinal(flags))
			sb.append("final ");
		if (AccessFlags.isSynthetic(flags))
			sb.append("synthetic ");
		if (AccessFlags.isSuper(flags))
			sb.append("super ");
		if (AccessFlags.isAnnotation(flags))
			sb.append("annotation ");
		if (AccessFlags.isInterface(flags))
			sb.append("interface ");
		if (AccessFlags.isAbstract(flags))
			sb.append("abstract ");
		if (AccessFlags.isEnum(flags))
			sb.append("enum ");
	}
	
	private void disassembleInterfaces(ClassConstant[] interfaces, StringBuffer sb) {
		if (interfaces.length > 0) {
			sb.append(".implements ");
			for (int i = 0; i < interfaces.length; i++) {
				if (i != 0)
					sb.append(" ");
				disassembleClassConstant(interfaces[i], sb);
			}
			sb.append("\n");
		}
	}	
	
	private void disassembleSignatureAttribute(SignatureAttribute signatureAttribute, StringBuffer sb) {
		sb.append(".signature ");
		disassembleUTF8Constant(signatureAttribute.getSignature(), sb);
		sb.append("\n");
	}

	private void disassembleEnclosingMethodAttribute(EnclosingMethodAttribute enclosingMethodAttribute, StringBuffer sb) {
		sb.append(".enclosing method ");
		//enclosingMethodAttribute.get
		sb.append("\n");
	}
	
	private void disassembleSourceFileAttribute(SourceFileAttribute sourceFileAttribute, StringBuffer sb) {
		sb.append(".source ");
		disassembleUTF8Constant(sourceFileAttribute.getSourceFile(), sb);
		sb.append("\n");
	}
	
	private void disassembleClassConstant(ClassConstant classConstant, StringBuffer sb) {
		disassembleUTF8Constant(classConstant.getName(), sb);
	}
	
	private void disassembleUTF8Constant(UTF8Constant utf8Constant, StringBuffer sb) {
		sb.append(utf8Constant.getValue());
	}
	
	private void disassembleField(Field field, StringBuffer sb) {
		sb.append(".field ");
		disassembleFieldAccessFlags(field.getAccessFlags(), sb);
		disassembleUTF8Constant(field.getName(), sb);
		sb.append(" ");
		disassembleUTF8Constant(field.getDescriptor(), sb);
		sb.append("\n");
		sb.append(".end field\n");
	}


	private void disassembleFieldAccessFlags(int flags, StringBuffer sb) {
		if (AccessFlags.isPublic(flags))
			sb.append("public ");
		if (AccessFlags.isProtected(flags))
			sb.append("protected ");
		if (AccessFlags.isPrivate(flags))
			sb.append("private ");
		if (AccessFlags.isStatic(flags))
			sb.append("static ");
		if (AccessFlags.isFinal(flags))
			sb.append("final ");
		if (AccessFlags.isVolatile(flags))
			sb.append("volatile ");
		if (AccessFlags.isTransient(flags))
			sb.append("transient ");
		if (AccessFlags.isSynthetic(flags))
			sb.append("synthetic ");
		if (AccessFlags.isEnum(flags))
			sb.append("enum ");
	}

	private void disassembleMethod(Method method, StringBuffer sb) {
		sb.append(".method ");
		disassembleMethodAccessFlags(method.getAccessFlags(), sb);
		disassembleUTF8Constant(method.getName(), sb);
		sb.append(" ");
		disassembleUTF8Constant(method.getDescriptor(), sb);
		sb.append("\n");
		CodeAttribute code = null;
		for(int i = 0; i < method.getAttributes().length; i++) {
			Attribute attribute = method.getAttributes()[i];
			if (attribute instanceof CodeAttribute)
				code = (CodeAttribute) attribute;
		}
		if (code != null)
			disassembleCodeAttribute(code, sb);
		sb.append(".end method\n");		
	}

	private void disassembleMethodAccessFlags(int flags, StringBuffer sb) {
		if (AccessFlags.isPublic(flags))
			sb.append("public ");
		if (AccessFlags.isProtected(flags))
			sb.append("protected ");
		if (AccessFlags.isPrivate(flags))
			sb.append("private ");
		if (AccessFlags.isStatic(flags))
			sb.append("static ");
		if (AccessFlags.isFinal(flags))
			sb.append("final ");
		if (AccessFlags.isSynchronized(flags))
			sb.append("synchronized ");
		if (AccessFlags.isBridge(flags))
			sb.append("bridge ");
		if (AccessFlags.isVarargs(flags))
			sb.append("varargs ");
		if (AccessFlags.isNative(flags))
			sb.append("native ");
		if (AccessFlags.isAbstract(flags))
			sb.append("abstract ");
		if (AccessFlags.isStrict(flags))
			sb.append("strict ");
		if (AccessFlags.isSynthetic(flags))
			sb.append("synthetic ");
	}

	private void disassembleCodeAttribute(CodeAttribute codeAttribute, StringBuffer sb) {
		sb.append(".limit locals ").append(codeAttribute.getMaxLocals()).append("\n");
		sb.append(".limit stack ").append(codeAttribute.getMaxStack()).append("\n");
		
		int pos;
		for (pos = 0; pos < codeAttribute.getByteCode().length;) {
			short bc;
			int format;
			sb.append(Utils.formatHex(pos, 3)).append(": ");
			bc = codeAttribute.getByteCode(pos++);
			format = BytecodeInstructions.byteCodeFormat[bc];
			if (format == BytecodeInstructions.FORMAT_INVALID) {
				//throw new RuntimeException("Invalid bytecode");
			} else if (format == BytecodeInstructions.FORMAT_WIDE) {

			} else {
				short arg1;
				int warg1;
				sb.append(BytecodeInstructions.names[bc]).append("\t");
				switch (format) {
				case BytecodeInstructions.FORMAT_NOARGS:
					break;
				case BytecodeInstructions.FORMAT_LOCALVAR:
					arg1 = codeAttribute.getByteCode(pos++);
					sb.append(Integer.toString(arg1));
					break;
				case BytecodeInstructions.FORMAT_CONSTANT:
					arg1 = codeAttribute.getByteCode(pos++);
					sb.append("#").append(Integer.toString(arg1));
					sb.append("// ").append(
							codeAttribute.getOnwerClass().getConstantPool().getConstant(
									arg1));
					break;
				case BytecodeInstructions.FORMAT_CONSTANT_W:
					warg1 = codeAttribute.getByteCode(pos) * 256
							+ codeAttribute.getByteCode(pos + 1);
					pos += 2;
					sb.append("#").append(Integer.toString(warg1));
					sb.append("// ").append(
							codeAttribute.getOnwerClass().getConstantPool().getConstant(
									warg1));
					break;
				case BytecodeInstructions.FORMAT_IINC:
				case BytecodeInstructions.FORMAT_BRANCH:
				case BytecodeInstructions.FORMAT_BRANCH_W:
				case BytecodeInstructions.FORMAT_TABLESWITCH:
				case BytecodeInstructions.FORMAT_LOOKUPSWITCH:
				case BytecodeInstructions.FORMAT_ARRAY:
				case BytecodeInstructions.FORMAT_MULTIARRAY:
					break;
				}
			}
			sb.append("\n");
		}
	}

}
