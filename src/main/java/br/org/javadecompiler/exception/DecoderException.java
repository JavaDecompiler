package br.org.javadecompiler.exception;

public class DecoderException extends BaseException {
	private static final long serialVersionUID = 2912436265952677672L;
	
	public DecoderException(String message) {
		super(message);
	}

}
