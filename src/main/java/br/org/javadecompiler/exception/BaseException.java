package br.org.javadecompiler.exception;

public abstract class BaseException extends Exception {
	
	public BaseException(String message) {
		super(message);
	}
	
}
