package br.org.javadecompiler.exception;

public class ClassReaderException extends BaseException {
	private static final long serialVersionUID = 1682695130157408937L;

	public ClassReaderException(String message) {
		super(message);
	}
}
