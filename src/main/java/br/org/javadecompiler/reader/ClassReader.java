package br.org.javadecompiler.reader;

import br.org.javadecompiler.exception.ClassReaderException;

public interface ClassReader {

	public int available();	
	public int position();
	public void reset();
	public void mark();
	public void rollback();
	
	public void skip(int bytes) throws ClassReaderException;
	public void read(byte b[], int off, int len) throws NullPointerException, IndexOutOfBoundsException, ClassReaderException;
	public int read() throws ClassReaderException;
	
	public long  readUnsignedInt() throws ClassReaderException;
	public int   readUnsignedShort() throws ClassReaderException;
	public short readUnsignedByte() throws ClassReaderException;
	public int   readSignedInt() throws ClassReaderException;
	public short readSignedShort() throws ClassReaderException;
	public byte  readSignedByte() throws ClassReaderException;
	
	public float  readFloat() throws ClassReaderException;
	public long   readSignedLong() throws ClassReaderException;
	public double readDouble() throws ClassReaderException;
	
	public String readUtf8() throws ClassReaderException;
}