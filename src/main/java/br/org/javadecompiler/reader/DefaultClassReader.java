package br.org.javadecompiler.reader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import br.org.javadecompiler.exception.ClassReaderException;

public final class DefaultClassReader extends AbstractClassReader {

	private final byte buf[];
	private int pos;
	private int count;
	private int first;
	private int mark;
	
	public DefaultClassReader(byte buf[]) {
		this.buf = buf;
		this.mark = this.first = this.pos = 0;
		this.count = buf.length;
	}

	public DefaultClassReader(byte buf[], int offset, int length) {
		this.buf = buf;
		this.mark = this.first = this.pos = offset;
		this.count = Math.min(offset + length, buf.length);
	}

	public int read() throws ClassReaderException {
		if (pos >= count) throw new ClassReaderException("End of class reached");
		return (buf[pos++] & 0xff);
	}

	public void read(byte b[], int off, int len) throws NullPointerException, IndexOutOfBoundsException, ClassReaderException {
		if (b == null) {
			throw new NullPointerException();
		} else if (off < 0 || len < 0 || len > b.length - off) {
			throw new IndexOutOfBoundsException();
		}
		if (len <= 0) return;
		if (pos >= count || pos + len > count) {
			throw new ClassReaderException("Can't read " + len + " bytes: end of class reached");
		}
		System.arraycopy(buf, pos, b, off, len);
		pos += len;
	}

	public void skip(int bytes) throws ClassReaderException {
		if (pos + bytes > count) {
			throw new ClassReaderException("Invalid skip: beyond the end of class");
		}
		if (bytes < 0) {
			return;
		}
		pos += bytes;
	}
	
	public int available() {
		return count - pos;
	}
	
	public void reset() {
		this.pos = this.first;
	}
	
	public void mark() {
		this.mark = this.pos;
	}
	
	public void rollback() {
		this.pos = this.mark;
	}
	
	public int position() {
		return this.pos;
	}
	

	public static DefaultClassReader getFromInputStream(InputStream is) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		int r;
		do {
			r = is.read(buffer);
			if (r == -1) break;
			baos.write(buffer, 0, r);
		} while(true);
		return new DefaultClassReader(baos.toByteArray());
	}	

}


