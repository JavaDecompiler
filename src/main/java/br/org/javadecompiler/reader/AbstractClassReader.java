package br.org.javadecompiler.reader;

import br.org.javadecompiler.exception.ClassReaderException;

public abstract class AbstractClassReader implements ClassReader {

	public long readUnsignedInt() throws ClassReaderException {
		if (available() <= 3)
			throw new ClassReaderException("Can't read unsigned int");
		long result = ((long) read()) << 24;
		result |= read() << 16;
		result |= read() << 8;
		result |= read();
		return result;
	}

	public int readUnsignedShort() throws ClassReaderException {
		if (available() <= 1)
			throw new ClassReaderException("Can't read unsigned short");
		int result = read() << 8;
		result |= read();
		return result;
	}
	
	public short readUnsignedByte() throws ClassReaderException {
		if (available() <= 0)
			throw new ClassReaderException("Can't read unsigned byte");
		return (short) read();
	}
	
	public int readSignedInt() throws ClassReaderException {
		if (available() <= 3)
			throw new ClassReaderException("Can't read signed int");
		int result = (read()) << 24;
		result |= read() << 16;
		result |= read() << 8;
		result |= read();
		return result;		
	}
	
	public short readSignedShort() throws ClassReaderException {
		if (available() <= 1)
			throw new ClassReaderException("Can't read signed short");
		short result = (short) (read() << 8);
		result |= read();
		return result;
	}

	public byte readSignedByte() throws ClassReaderException {
		if (available() <= 0)
			throw new ClassReaderException("Can't read signed byte");
		return (byte) read();
	}
	
	public float readFloat() throws ClassReaderException {
		if (available() <= 3)
			throw new ClassReaderException("Can't read float");
		int bits = (read()) << 24;
		bits |= read() << 16;
		bits |= read() << 8;
		bits |= read();
		return Float.intBitsToFloat(bits);
	}
	
	public long readSignedLong() throws ClassReaderException {
		if (available() <= 7)
			throw new ClassReaderException("Can't read signed long");
		long result = (long) read();
		result = result << 8 | ((long) read());
		result = result << 8 | ((long) read());
		result = result << 8 | ((long) read());
		result = result << 8 | ((long) read());
		result = result << 8 | ((long) read());
		result = result << 8 | ((long) read());
		result = result << 8 | ((long) read());
		return result;
	}

	public double readDouble() throws ClassReaderException {
		if (available() <= 7)
			throw new ClassReaderException("Can't read double");
		long bits = (long) read();
		bits = bits << 8 | ((long) read());
		bits = bits << 8 | ((long) read());
		bits = bits << 8 | ((long) read());
		bits = bits << 8 | ((long) read());
		bits = bits << 8 | ((long) read());
		bits = bits << 8 | ((long) read());
		bits = bits << 8 | ((long) read());
		return Double.longBitsToDouble(bits);
	}
	
	

	public String readUtf8() throws ClassReaderException {
		StringBuffer result = new StringBuffer();
		int length = readUnsignedShort();
		if (available() < length)
			throw new ClassReaderException("Can't read (Java)UTF-8 string");
		for(int i = 0; i < length; i++) {
			byte x, y, z;
			char c;
			x = (byte) read();
			if ((x & 0x80) != 0) {
				if (((x & 0x40) == 0) || (((x & 0x20) != 0) && ((x & 0x10) != 0)))
					throw new ClassReaderException("Invalid first byte of an (Java)UTF-8 string character");

				if (i == (length - 1))
					throw new ClassReaderException("Missing 2nd byte of an (Java)UTF-8 string's character");

				y = (byte) read(); i++;
				if ((byte) (y & 0xC0) != (byte) 0x80)
					throw new ClassReaderException("Invalid 2nd byte of an (Java)UTF-8 string's character");

				if ((x & 0x20) != 0) {
					if (i == (length - 1))
						throw new ClassReaderException("Missing 3rd byte of an (Java)UTF-8 string's character");

					z = (byte) read(); i++;
					if ((byte) (z & 0xC0) != (byte) 0x80)
						throw new ClassReaderException("Invalid 3rd byte of an (Java)UTF-8 string's character");
					
					c = (char) (((x & 0x0f) << 12) | ((y & 0x3f) << 6) | (z & 0x3f)) ;
					if (c < '\u0800')
						throw new ClassReaderException("Invalid 3-byte character of an (Java)UTF-8 string");
				} else {
					c = (char) (((x & 0x1f) << 6) | (y & 0x3f)) ;
					if (c > '\u0000' && c < '\u0080')
						throw new ClassReaderException("Invalid 2-byte character of an (Java)UTF-8 string");
				}
			} else {
				c = (char) x;
				if (c == '\u0000')
					throw new ClassReaderException("Null byte in an (Java)UTF-8 string");
			}
			result.append(c);
		}
		return result.toString();
	}

}
