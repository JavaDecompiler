
public class TestFields {
	private final boolean fieldBooleanFalse = false;
	private final boolean fieldBooleanTrue  = true;
	private final char    fieldChar = 'a';
	private final char    fieldCharZero = '\0';
	private final short   fieldShort = 32767;
	private final int     fieldInt = 0;
	private final float   fieldFloat0 = 0.0f;
	private final float   fieldFloat1 = 1.0f;
	private final float   fieldFloatNaN = 0.0f / 0.0f;
	private final float   fieldFloatPositiveInfinity = 1.0f / 0.0f;
	private final float   fieldFloatNegativeInfinity = -1.0f / 0.0f;
	private final long    fieldLong0 = 0L;
	private final long    fieldLong1 = 1L;
	private final long    fieldLongMax = 0x7fffffffffffffffL;
	private final long    fieldLongMin = 0x8000000000000000L;
	private final double  fieldDouble0 = 0.0;
	private final double  fieldDouble1 = 1.0;
	private final double  fieldDoubleNaN = 0.0 / 0.0;
	private final double  fieldDoublePositiveInfinity = 1.0 / 0.0;
	private final double  fieldDoubleNegativeInfinity = -1.0 / 0.0;
}
