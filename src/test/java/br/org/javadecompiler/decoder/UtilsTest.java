package br.org.javadecompiler.decoder;

import junit.framework.TestCase;

public class UtilsTest extends TestCase {

	public UtilsTest(String testName) {
		super(testName);
	}

	public void testFormatHexInt() throws Exception {
		assertEquals("0", Utils.formatHex(0, 1));
		assertEquals("0", Utils.formatHex(0, 0));
		assertEquals("0", Utils.formatHex(0, -1));
		assertEquals("0", Utils.formatHex(0, -10));
		assertEquals("00", Utils.formatHex(0, 2));
		assertEquals("000", Utils.formatHex(0, 3));
		
		assertEquals("10", Utils.formatHex(0x10, 2));
		assertEquals("10", Utils.formatHex(0x10, 1));
		assertEquals("10", Utils.formatHex(0x10, 0));
		assertEquals("10", Utils.formatHex(0x10, -1));
		assertEquals("010", Utils.formatHex(0x10, 3));
		assertEquals("0010", Utils.formatHex(0x10, 4));
		
		assertEquals("ffffffff", Utils.formatHex(-1, 0));
		
		assertEquals("cafebabe", Utils.formatHex((int) 0xcafebabe, 0));
		assertEquals("00000000cafebabe", Utils.formatHex((int) 0xcafebabe, 16));
	}
	
	public void testFormatHexLong() throws Exception {
		assertEquals("0", Utils.formatHex(0L, 1));
		assertEquals("0", Utils.formatHex(0L, 0));
		assertEquals("0", Utils.formatHex(0L, -1));
		assertEquals("0", Utils.formatHex(0L, -10));
		assertEquals("00", Utils.formatHex(0L, 2));
		assertEquals("000", Utils.formatHex(0L, 3));
		
		assertEquals("10", Utils.formatHex(0x10L, 2));
		assertEquals("10", Utils.formatHex(0x10L, 1));
		assertEquals("10", Utils.formatHex(0x10L, 0));
		assertEquals("10", Utils.formatHex(0x10L, -1));
		assertEquals("010", Utils.formatHex(0x10L, 3));
		assertEquals("0010", Utils.formatHex(0x10L, 4));
		
		
		assertEquals("ffffffffffffffff", Utils.formatHex(-1L, 0));
		
		assertEquals("cafebabe", Utils.formatHex(0xcafebabeL, 0));
		assertEquals("00000000cafebabe", Utils.formatHex(0xcafebabeL, 16));
		assertEquals("cafedeadcafebabe", Utils.formatHex(0xcafedeadcafebabeL, 16));
	}
	
	public void testPrintByteArray() throws Exception {
		byte[] bytes = new byte[130];
		for(int i = 0; i < bytes.length; i++)
			bytes[i] = (byte) i;
		String result = Utils.printByteArray(bytes);
		String expected =
			"00000000: 00 01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f \n" +
			"00000010: 10 11 12 13 14 15 16 17 18 19 1a 1b 1c 1d 1e 1f \n" +
			"00000020: 20 21 22 23 24 25 26 27 28 29 2a 2b 2c 2d 2e 2f \n" + 
			"00000030: 30 31 32 33 34 35 36 37 38 39 3a 3b 3c 3d 3e 3f \n" +
			"00000040: 40 41 42 43 44 45 46 47 48 49 4a 4b 4c 4d 4e 4f \n" +
			"00000050: 50 51 52 53 54 55 56 57 58 59 5a 5b 5c 5d 5e 5f \n" +
			"00000060: 60 61 62 63 64 65 66 67 68 69 6a 6b 6c 6d 6e 6f \n" +
			"00000070: 70 71 72 73 74 75 76 77 78 79 7a 7b 7c 7d 7e 7f \n" +
			"00000080: 80 81 \n";
		assertEquals(expected, result);
	}

}
