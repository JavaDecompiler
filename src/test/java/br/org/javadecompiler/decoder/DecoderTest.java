package br.org.javadecompiler.decoder;

import java.io.InputStream;

import br.org.javadecompiler.classfile.JavaClass;
import br.org.javadecompiler.reader.ClassReader;
import br.org.javadecompiler.reader.DefaultClassReader;

import junit.framework.TestCase;

public class DecoderTest extends TestCase {
	public DecoderTest(String testName) {
		super(testName);
	}
	
	private static ClassReader readClass(Class clazz) throws Exception {
		String fileName = "/" + clazz.getName().replace('.', '/') + ".class";
		InputStream is = clazz.getResourceAsStream(fileName);
		return DefaultClassReader.getFromInputStream(is);
	}
	
	public static JavaClass decodeClass(Class clazz) throws Exception {
		JavaClass result = Decoder.decodeClass(readClass(clazz));
		return result;
	}

	public void testDecodeThis() throws Exception {
		decodeClass(this.getClass());
	}
	
	public void testDecodeJavaLangObject() throws Exception {
		decodeClass(java.lang.Object.class);
	}

	public void testDecodeJavaLangClass() throws Exception {
		decodeClass(java.lang.Class.class);
	}

	public void testDecodeInnerClasses() throws Exception {
		decodeClass(br.org.javadecompiler.classfile.attribute.LineNumberTableAttribute.LineNumber.class);
	}
	
	public void testDecodeAnonymousInnerClasses() throws Exception {
		Object object = new br.org.javadecompiler.classfile.attribute.AttributeOwner() {
			public br.org.javadecompiler.classfile.attribute.Attribute[] getAttributes() {
				return null;
			}
		};
		decodeClass(object.getClass());
		
	}
}
