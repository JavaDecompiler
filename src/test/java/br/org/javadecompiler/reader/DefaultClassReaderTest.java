package br.org.javadecompiler.reader;

import br.org.javadecompiler.exception.ClassReaderException;
import junit.framework.TestCase;

public class DefaultClassReaderTest extends TestCase {
	private DefaultClassReader reader;
	
	public DefaultClassReaderTest(String testName) {
		super(testName);
	}
	
	protected void setUp() throws Exception {
		super.setUp();
		reader = DefaultClassReader.getFromInputStream(getClass().getResourceAsStream("/classes/test.bin"));
	}
	
	protected void tearDown() throws Exception {
		reader = null;
	}
	
	public void testAvailable() throws Exception {
		assertEquals(542, reader.available());
	}
	
	public void testPosition() throws Exception {
		assertEquals(0, reader.position());
	}
	
	public void testSkip() throws Exception {
		reader.skip(12);
		assertEquals(12, reader.position());
		reader.skip(3);
		assertEquals(15, reader.position());
		reader.skip(-2);
		assertEquals(15, reader.position());
	}

	public void testReset() throws Exception {
		reader.skip(12);
		assertEquals(12, reader.position());
		reader.reset();
		assertEquals(0, reader.position());
	}

	public void testMarkAndRollback() throws Exception {
		reader.skip(12);
		reader.mark();
		assertEquals(12, reader.position());
		reader.reset();
		assertEquals(0, reader.position());
		reader.rollback();
		assertEquals(12, reader.position());
		reader.skip(32);
		assertEquals(44, reader.position());
		reader.rollback();
		assertEquals(12, reader.position());
	}

	public void testReadUnsignedByte() throws Exception {
		assertEquals((short) 0x00, reader.readUnsignedByte());
		assertEquals((short) 0x7f, reader.readUnsignedByte());
		assertEquals((short) 0x80, reader.readUnsignedByte());
		assertEquals((short) 0xff, reader.readUnsignedByte());
	}

	public void testReadSignedByte() throws Exception {
		assertEquals((byte)    0, reader.readSignedByte());
		assertEquals((byte)  127, reader.readSignedByte());
		assertEquals((byte) -128, reader.readSignedByte());
		assertEquals((byte)   -1, reader.readSignedByte());
	}

	public void testReadUnsignedShort() throws Exception {
		reader.skip(4);
		assertEquals(0x0000, reader.readUnsignedShort());
		assertEquals(0x7fff, reader.readUnsignedShort());
		assertEquals(0x8000, reader.readUnsignedShort());
		assertEquals(0xffff, reader.readUnsignedShort());
	}

	public void testReadSignedShort() throws Exception {
		reader.skip(4);
		assertEquals((short)      0, reader.readSignedShort());
		assertEquals((short)  32767, reader.readSignedShort());
		assertEquals((short) -32768, reader.readSignedShort());
		assertEquals((short)     -1, reader.readSignedShort());
	}

	public void testReadUnsignedInt() throws Exception {
		reader.skip(12);
		assertEquals(0x00000000L, reader.readUnsignedInt());
		assertEquals(0x7fffffffL, reader.readUnsignedInt());
		assertEquals(0x80000000L, reader.readUnsignedInt());
		assertEquals(0xffffffffL, reader.readUnsignedInt());
	}

	public void testReadSignedInt() throws Exception {
		reader.skip(12);
		assertEquals(          0, reader.readSignedInt());
		assertEquals( 2147483647, reader.readSignedInt());
		assertEquals(-2147483648, reader.readSignedInt());
		assertEquals(         -1, reader.readSignedInt());
	}
	
	public void testRead() throws Exception {
		reader.skip(28);
		byte buf[] = new byte[4];
		reader.read(buf, 0, 4);
		assertEquals((byte) 0xDE, buf[0]);
		assertEquals((byte) 0xAD, buf[1]);
		assertEquals((byte) 0xCA, buf[2]);
		assertEquals((byte) 0xFE, buf[3]);
	}
	
	public void testReadNullBuffer() throws Exception {
		reader.skip(28);
		try {
			reader.read(null, 0, 4);
			fail("Expecting a NullPointerException");
		} catch (NullPointerException ex) {
		}
	}
		
	public void testReadNegativeLength() throws Exception {
		reader.skip(28);
		try {
			byte buf[] = new byte[4];
			reader.read(buf, 0, -4);
			fail("Expecting a IndexOutOfBoundsException");
		} catch (IndexOutOfBoundsException ex) {
		}
	}

	public void testReadNegativeOffset() throws Exception {
		reader.skip(28);
		try {
			byte buf[] = new byte[4];
			reader.read(buf, -1, 4);
			fail("Expecting a IndexOutOfBoundsException");
		} catch (IndexOutOfBoundsException ex) {
		}
	}

	public void testReadOutOfBounds() throws Exception {
		reader.skip(28);
		try {
			byte buf[] = new byte[4];
			reader.read(buf, 1, 4);
			fail("Expecting a IndexOutOfBoundsException");
		} catch (IndexOutOfBoundsException ex) {
		}
	}

	public void testReadUtf8() throws Exception {
		reader.skip(32);
		String result = reader.readUtf8();
		assertEquals("Simple string!", result);
		result = reader.readUtf8();
		assertEquals("0000\u0000" + "07ff\u07ff" + "0080\u0080" + "00ff\u00ff" + "07c0\u07c0", result);
		result = reader.readUtf8();
		assertEquals("0800\u0800" + "ffff\uffff", result);
	}
	
	
	private void readUtf8Invalid(int pos) {
		reader.reset();
		boolean ok = false;
		try {
			reader.skip(pos);
			ok = true;
			reader.readUtf8();
			fail("Expecting an exception here");
		} catch(ClassReaderException ex) {
			//System.out.println(ex.getMessage());
		} catch(Throwable t) {
			fail("Invalid exception: " + t);
		}
		assertTrue(ok);
	}
	
	public void testReadUtf8Invalid() throws Exception {
		for(int i = 0; i < 17; i++)
			readUtf8Invalid(96 + 16 * i);
	}
	
	public void testReadFloat() throws Exception {
		reader.skip(354);
		assertEquals(Float.POSITIVE_INFINITY, reader.readFloat(), 0.0f);
		assertEquals(Float.NEGATIVE_INFINITY, reader.readFloat(), 0.0f);
		assertTrue(Float.isNaN(reader.readFloat()));
		assertTrue(Float.isNaN(reader.readFloat()));
		assertTrue(Float.isNaN(reader.readFloat()));
		assertTrue(Float.isNaN(reader.readFloat()));
		assertEquals(0.0f, reader.readFloat(), 0.0f);
		assertEquals(Float.MIN_VALUE, reader.readFloat(), 0.0f);
		assertEquals(1.1754942E-38f, reader.readFloat(), 0.0f);
		assertEquals(-0.0f, reader.readFloat(), 0.0f);
		assertEquals(-1.4E-45f, reader.readFloat(), 0.0f);
		assertEquals(-1.1754942E-38f, reader.readFloat(), 0.0f);
		assertEquals(1.17549435E-38f, reader.readFloat(), 0.0f);
		assertEquals(2.3509886E-38f, reader.readFloat(), 0.0f);
		assertEquals(1.7014117E38f, reader.readFloat(), 0.0f);
	}

	public void testReadDouble() throws Exception {
		reader.skip(414);
		assertEquals(Double.POSITIVE_INFINITY, reader.readDouble(), 0.0);
		assertEquals(Double.NEGATIVE_INFINITY, reader.readDouble(), 0.0);
		assertTrue(Double.isNaN(reader.readDouble()));
		assertTrue(Double.isNaN(reader.readDouble()));
		assertTrue(Double.isNaN(reader.readDouble()));
		assertTrue(Double.isNaN(reader.readDouble()));
		assertEquals(0.0, reader.readDouble(), 0.0);
		assertEquals(-0.0, reader.readDouble(), 0.0);
		assertEquals(Double.MIN_VALUE, reader.readDouble(), 0.0);
		assertEquals(3.141592653589793, reader.readDouble(), 0.0);
		assertEquals(-3.141592653589793, reader.readDouble(), 0.0);
	}		
		
	public void testReadLong() throws Exception {
		reader.skip(502);
		assertEquals(0L, reader.readSignedLong());
		assertEquals(1L, reader.readSignedLong());
		assertEquals(Long.MAX_VALUE, reader.readSignedLong());
		assertEquals(Long.MIN_VALUE, reader.readSignedLong());
		assertEquals(-1L, reader.readSignedLong());
	}
}
