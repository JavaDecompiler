package br.org.javadecompiler.disassembler;

import br.org.javadecompiler.classfile.JavaClass;
import br.org.javadecompiler.decoder.Decoder;
import br.org.javadecompiler.decoder.DecoderTest;
import br.org.javadecompiler.reader.DefaultClassReader;
import junit.framework.TestCase;

public class JasminDisassemblerTest extends TestCase {
	public JasminDisassemblerTest(String testName) {
		super(testName);
	}
	
	public void testDisassembleClass() throws Exception {
		JavaClass clazz = DecoderTest.decodeClass(this.getClass());
		JasminDisassembler disassembler = new JasminDisassembler();
		System.out.println(disassembler.disassembleClass(clazz));
	}

	public void testDisassembleClassTestFields() throws Exception {
		JavaClass clazz = Decoder.decodeClass(DefaultClassReader.getFromInputStream(getClass().getResourceAsStream("/classes/TestFields.class")));
		JasminDisassembler disassembler = new JasminDisassembler();
		System.out.println(disassembler.disassembleClass(clazz));
	}

}
